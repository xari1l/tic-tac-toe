import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe {
    static int playerCount = 1;
    static int p1Wins=0;
    static int p2Wins=0;

    private static JLabel scoreLabel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Tic Tac Toe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        JButton[][] buttons = new JButton[3][3];

        JPanel panel = new JPanel();
        frame.add(scoreLabel = new JLabel("Score "+p1Wins+"-"+p2Wins), BorderLayout.NORTH);
        scoreLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        panel.setLayout(new GridLayout(3, 3));

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j] = new JButton();
                panel.add(buttons[i][j]);

                final int row = i;
                final int col = j;
                buttons[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (playerCount % 2 == 1) {
                            buttons[row][col].setText("X");
                        } else {
                            buttons[row][col].setText("O");
                        }
                        playerCount++;
                        buttons[row][col].setEnabled(false);
                        if (checkForWinner(buttons, row, col)) {
                            JOptionPane.showMessageDialog(frame, "Player " + (playerCount%2 + 1) + " wins!");

                            if((playerCount%2+1)==1){
                                p1Wins++;
                            }else if((playerCount%2+1)==2){
                                p2Wins++;
                            }
                            resetBoard(buttons);
                            scoreLabel.setText("Score "+p1Wins+"-"+p2Wins);
                        }
                        if(playerCount==9){
                            JOptionPane.showMessageDialog(frame,"Draw");
                            resetBoard(buttons);
                        }
                    }
                });
            }
        }

        frame.add(panel);
        frame.setVisible(true);
    }

    public static boolean checkForWinner(JButton[][] buttons, int row, int col) {
        String player = buttons[row][col].getText();

        boolean isWinner = false;

        // Check rows
        for (int i = 0; i < 3; i++) {
            if (buttons[i][0].getText().equals(buttons[i][1].getText()) &&
                    buttons[i][1].getText().equals(buttons[i][2].getText()) &&
                    !buttons[i][0].getText().equals("")) {
                isWinner = true;
                break;
            }
        }

        // Check columns
        if (!isWinner) {
            for (int i = 0; i < 3; i++) {
                if (buttons[0][i].getText().equals(buttons[1][i].getText()) &&
                        buttons[1][i].getText().equals(buttons[2][i].getText()) &&
                        !buttons[0][i].getText().equals("")) {
                    isWinner = true;
                    break;
                }
            }
        }

        // Check diagonals
        if (!isWinner) {
            if (buttons[0][0].getText().equals(buttons[1][1].getText()) &&
                    buttons[1][1].getText().equals(buttons[2][2].getText()) &&
                    !buttons[0][0].getText().equals("")) {
                isWinner = true;
            } else if (buttons[0][2].getText().equals(buttons[1][1].getText()) &&
                    buttons[1][1].getText().equals(buttons[2][0].getText()) &&
                    !buttons[0][2].getText().equals("")) {
                isWinner = true;
            }
        }
        return isWinner;
    }


    public static void resetBoard(JButton[][] buttons) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
                buttons[i][j].setEnabled(true);
            }
        }
        playerCount=1;
    }


}